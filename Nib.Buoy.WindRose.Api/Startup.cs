﻿using System.IO;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Nib.Buoy.WindRose.Api.Core;
using Nib.Buoy.WindRose.Api.Core.Services;
using Nib.Buoy.WindRose.Api.Hubs;

namespace Nib.Buoy.WindRose {
    public class Startup {
        private readonly ILogger<Startup> _logger;
        private readonly IConfiguration configuration;
        public Startup (
            ILogger<Startup> logger,
            IHostingEnvironment env,
            IConfiguration configuration
        ) {
            this._logger = logger;
            this.configuration = configuration;
            var builder = new ConfigurationBuilder ()
                .SetBasePath (env.ContentRootPath)
                .AddJsonFile ("appsettings.json", optional : false, reloadOnChange : true)
                .AddJsonFile ($"appsettings.{env.EnvironmentName}.json", optional : true)
                .AddEnvironmentVariables ();
            Configuration = builder.Build ();
        }

        public Startup (IConfiguration configuration) {
            this.Configuration = configuration;

        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
            services.AddScoped<IDataBroadcaster, DataBroadcaster> ();
            services.AddSingleton<Microsoft.Extensions.Hosting.IHostedService, DataSchedulerService> ();
            services.AddSingleton<IConfiguration> (Configuration);
            services.AddSingleton<IHubClientsCount, HubClientsCount> ();

            _logger.LogWarning ("StartUp()");

            services.AddSignalR ()
                .AddHubOptions<BuoyDataHub> (options => {
                    options.EnableDetailedErrors = true;
                })
                .AddJsonProtocol ();

            services.AddHealthChecks ().AddDbContextCheck<BuoyDbContext> ();
            services.AddDbContextPool<BuoyDbContext> (options => {
                options
                    .UseMySql (Configuration.GetConnectionString ("Default"), mysqlOptions => {
                        mysqlOptions
                            .CharSetBehavior (
                                Pomelo.EntityFrameworkCore.MySql.Infrastructure.CharSetBehavior.NeverAppend)
                            .AnsiCharSet (Pomelo.EntityFrameworkCore.MySql.Infrastructure.CharSet.Latin1)
                            .UnicodeCharSet (Pomelo.EntityFrameworkCore.MySql.Infrastructure.CharSet.Utf8mb4);
                    })
                    .EnableSensitiveDataLogging (true);
            });

            services.AddMvc ().SetCompatibilityVersion (CompatibilityVersion.Version_2_2);

            services.AddCors (options => {
                options.AddPolicy ("AllowAllCorsPolicy",
                    builder => {
                        builder
                            .AllowAnyOrigin ()
                            .AllowAnyMethod ()
                            .AllowAnyHeader ();
                    });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            }

            app.UseHealthChecks ("/appStatus", new HealthCheckOptions {
                ResponseWriter = async (context, report) => {
                    var status = report.Status.ToString ();
                    switch (report.Status) {
                        case Microsoft.Extensions.Diagnostics.HealthChecks.HealthStatus.Healthy:
                            status = "APP_STATUS_OK";
                            break;
                        case Microsoft.Extensions.Diagnostics.HealthChecks.HealthStatus.Degraded:
                            status = "APP_STATUS_DEGRADED";
                            break;
                        case Microsoft.Extensions.Diagnostics.HealthChecks.HealthStatus.Unhealthy:
                        default:
                            status = "APP_STATUS_FAILURE";
                            break;
                    }
                    var bytes = Encoding.UTF8.GetBytes (status);
                    context.Response.ContentType = "text/plain";
                    await context.Response.Body.WriteAsync (bytes);
                }
            });

            app.UseCors ("AllowAllCorsPolicy");

            var wsRoot = System.Environment.GetEnvironmentVariable("NIB_WINDROSE_WS_ROOT");
            app.UseSignalR (routes => {
                var wsPath  = (string.IsNullOrEmpty(wsRoot) || wsRoot == "/") ? "/ws" : $"{wsRoot}/ws";
                _logger.LogWarning ("Using WS path [{0}]", wsPath);
                routes.MapHub<BuoyDataHub> (wsPath);
            });

            if (env.IsDevelopment ()) {
                var staticWebPath = (string.IsNullOrEmpty(wsRoot) || wsRoot == "/") ? "/static" : $"{wsRoot}/static";
                var wwwrootProvider = new PhysicalFileProvider (Path.Combine (Directory.GetCurrentDirectory (), "wwwroot"));
                _logger.LogWarning ("Mapping directory [{0}] -> [{1}]", wwwrootProvider.Root, staticWebPath);
                app.UseFileServer (new FileServerOptions {
                    FileProvider = wwwrootProvider,
                        RequestPath = staticWebPath,
                        EnableDirectoryBrowsing = true
                });
            }

            app.UseMvc ();
        }
    }
}
