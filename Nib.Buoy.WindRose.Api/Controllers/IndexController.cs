using Microsoft.AspNetCore.Mvc;

namespace Nib.Buoy.WindRose.Api.Controllers {
    public class IndexController : Controller {
        public IndexController () { }

        [HttpGet ("/")]
        [Produces("text/plain")]
        public string Index () {
             return "";
        }
    }
}
