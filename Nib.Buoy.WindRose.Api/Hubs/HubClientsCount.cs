namespace Nib.Buoy.WindRose.Api.Hubs {
    public class HubClientsCount : IHubClientsCount {
        public HubClientsCount () { }

        public int ClientsCount { get; private set; }

        public void Connect() {
            ClientsCount++;
        }
        public void Disconnect() {
            ClientsCount--;
        }
    }
}