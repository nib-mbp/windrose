using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace Nib.Buoy.WindRose.Api.Hubs {
    public class BuoyDataHub : Hub {
        private readonly ILogger<BuoyDataHub> _logger;
        private readonly IHubClientsCount _clientsCount;

        public BuoyDataHub (
            ILogger<BuoyDataHub> logger,
            IHubClientsCount clientsCount
        ) {
            _logger = logger;
            this._clientsCount = clientsCount;
        }
        public override Task OnConnectedAsync () {
            _clientsCount.Connect();
            _logger.LogTrace("WS Connect - ConnectedClients={num}", _clientsCount.ClientsCount);
            return base.OnConnectedAsync ();
        }

        public override Task OnDisconnectedAsync (Exception exception) {
            _clientsCount.Disconnect();
            _logger.LogTrace("WS Disconnect - ConnectedClients={num}", _clientsCount.ClientsCount);
            return base.OnDisconnectedAsync (exception);
        }
    }
}