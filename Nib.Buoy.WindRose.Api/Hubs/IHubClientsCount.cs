namespace Nib.Buoy.WindRose.Api.Hubs {
    public interface IHubClientsCount {
        int ClientsCount { get; }
        void Connect ();
        void Disconnect ();

    }
}