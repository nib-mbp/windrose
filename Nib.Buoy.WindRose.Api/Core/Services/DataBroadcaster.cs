using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Nib.Buoy.WindRose.Api.Core.Models;
using Nib.Buoy.WindRose.Api.Hubs;

namespace Nib.Buoy.WindRose.Api.Core.Services {
    public class DataBroadcaster : IDataBroadcaster {
        private readonly ILogger<DataBroadcaster> _logger;
        private readonly BuoyDbContext _context;
        private readonly IHubClientsCount _hubClientsCount;

        public DataBroadcaster (
            ILogger<DataBroadcaster> logger,
            BuoyDbContext context,
            IHubContext<BuoyDataHub> hub,
            IHubClientsCount hubClientsCount
        ) {
            this._logger = logger;
            this._context = context;
            this.Hub = hub;
            this._hubClientsCount = hubClientsCount;
        }

        private IHubContext<BuoyDataHub> Hub { get; set; }

        public async Task Process () {
            if (_hubClientsCount.ClientsCount > 0) {
                var means15 = await _context.Buoy15MinMeans.AsNoTracking ().SingleOrDefaultAsync ();
                var means30 = await _context.Buoy30MinMeans.AsNoTracking ().SingleOrDefaultAsync ();
                var lastWindData = (
                        await _context.WindCurrentData.AsNoTracking ()
                        .FromSql (
                            "CALL p_getCurrentBuoyData(@dt, @speed, @direction); " +
                            "SELECT UTC_TIMESTAMP() AS `ServerDt`, TIMESTAMP(@dt) AS `WindCurrentDt`, @speed AS `WindCurrentSpeed`, @direction AS `WindCurrentDirection`")
                        .ToListAsync ())
                    .FirstOrDefault ();
                var buoyData = new BuoyAllData (means15, means30, lastWindData);
                await Hub.Clients.All.SendAsync ("data", buoyData);
                // _logger.LogDebug ("buoy [{data}]", JsonConvert.SerializeObject (buoyData, Formatting.Indented));
            }
        }
    }
}
