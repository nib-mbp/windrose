using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NCrontab;
using static NCrontab.CrontabSchedule;

namespace Nib.Buoy.WindRose.Api.Core.Services {
    // Scheduler should be a 6 patck crontab (exetension to standard one)
    // * * * * * *
    // - - - - - -
    // | | | | | |
    // | | | | | +--- day of week (0 - 6) (Sunday=0)
    // | | | | +----- month (1 - 12)
    // | | | +------- day of month (1 - 31)
    // | | +--------- hour (0 - 23)
    // | +----------- min (0 - 59)
    // +------------- sec (0 - 59)
    public class DataSchedulerService : BackgroundService {
        // Default data dispatch schedule - every 2 seconds
        private const string defaultDataDispatchSchedule = "*/2 * * * * *";
        private readonly ILogger<DataSchedulerService> _logger;
        private readonly IServiceProvider _serviceProvider;
        private readonly IConfiguration _configuration;
        private CrontabSchedule _schedule = null;
        private DateTime _nextRun;

        public DataSchedulerService (
            ILogger<DataSchedulerService> logger,
            IServiceProvider serviceProvider,
            IConfiguration configuration
        ) {
            this._logger = logger;
            this._serviceProvider = serviceProvider;
            this._configuration = configuration;
            string dataDispatchSchedule = "";
            try {
                dataDispatchSchedule = _configuration.GetValue("BroadcastSchedule", defaultDataDispatchSchedule);
                _schedule = CrontabSchedule.TryParse (dataDispatchSchedule, new ParseOptions { IncludingSeconds = true });
                if (_schedule == null) {
                    _logger.LogWarning("ATTENTION: Failed parsing scheduler [{schedule}]! Failing back to '* * * * *' (every minute)!", dataDispatchSchedule);
                    _schedule = CrontabSchedule.Parse("* * * * *");
                }
                _nextRun = _schedule.GetNextOccurrence (DateTime.Now);
                _logger.LogDebug ("data dispatch schedule ['{schedule}'] / next run [{nextRun}]", _schedule.ToString (), _nextRun);
            } catch (NCrontab.CrontabException ex) {
                _schedule = null;
                _logger.LogError (ex, "Failed parsing crontab '{cron}'", dataDispatchSchedule);
            }
        }

        protected override async Task ExecuteAsync (CancellationToken stoppingToken) {
            _logger.LogDebug ("GracePeriodManagerService is starting.");

            stoppingToken.Register (() =>
                _logger.LogDebug ("Scheduler {name} -> STOPPING", MethodBase.GetCurrentMethod ().DeclaringType.Name));

            if (_schedule == null)
                return;

            while (!stoppingToken.IsCancellationRequested) {
                var now = DateTime.Now;
                var nextrun = _schedule.GetNextOccurrence (now);
                if (now > _nextRun) {
                    using (var scope = _serviceProvider.CreateScope ()) {
                        var dataBroadcaster = scope.ServiceProvider.GetService<IDataBroadcaster> ();
                        try {
                            await dataBroadcaster.Process ();
                        } catch (Exception ex) {
                            _logger.LogError (ex, "Failed executing DataBroadcaster::Process()!");
                            break;
                        }

                    }

                    _nextRun = _schedule.GetNextOccurrence (DateTime.Now);
                }
                await Task.Delay (200, stoppingToken); // 200 ms
            }
            _logger.LogDebug ("Scheduler {name} -> GOT CANCELATION REQUEST", this.GetType ().Name);
        }
    }
}