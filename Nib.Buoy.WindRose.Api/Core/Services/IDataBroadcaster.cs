using System.Threading.Tasks;

namespace Nib.Buoy.WindRose.Api.Core.Services
{
    public interface IDataBroadcaster
    {
         Task Process ();
    }
}