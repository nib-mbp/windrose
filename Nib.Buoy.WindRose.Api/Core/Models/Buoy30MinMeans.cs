using System;

/*
DROP VIEW IF EXISTS v_means30;
CREATE VIEW v_means30 AS
  SELECT
    p.dateend AS WavesDateEnd,
    ROUND(IFNULL(w.wave_height,-99), 1) AS WavesHeight, ROUND(IFNULL(w.mean_direction,-99)) AS WavesDirection, ROUND(IFNULL(w.mean_period,-99), 1) AS WavesPeriod
    FROM buoy_means.`awac_waves` w
      LEFT JOIN buoy_means.`profile` p ON w.pid=p.pid
    WHERE p.`period_length`='30'
    ORDER BY w.id DESC
    LIMIT 1;
*/
namespace Nib.Buoy.WindRose.Api.Core.Models {
    public class Buoy30MinMeans : IBuoy30MinMeans {
        public DateTime WavesDateEnd { get; set; }
        public double? WavesHeight { get; set; }
        public double? WavesDirection { get; set; }
        public double? WavesPeriod { get; set; }

        public Buoy30MinMeans () { }
    }
}
