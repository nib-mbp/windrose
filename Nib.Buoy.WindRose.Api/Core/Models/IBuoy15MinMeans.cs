using System;

namespace Nib.Buoy.WindRose.Api.Core.Models {
    public interface IBuoy15MinMeans {
        UInt32 Pid { get; set; }
        DateTime WindDateEnd { get; set; }
        double? WindMeanSpeed { get; set; }
        double? WindMeanDirection { get; set; }
        int? WindBeaufortSpeed { get; set; }
        double? SeaSurfaceTemp { get; set; }
        double? AirTemp { get; set; }
    }
}
