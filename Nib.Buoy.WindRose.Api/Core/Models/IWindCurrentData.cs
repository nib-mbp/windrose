using System;

namespace Nib.Buoy.WindRose.Api.Core.Models {
    public interface IWindCurrentData {
        DateTime? ServerDt { get; set; }
        DateTime? WindCurrentDt { get; set; }
        double? WindCurrentSpeed { get; set; }
        double? WindCurrentDirection { get; set; }
    }
}