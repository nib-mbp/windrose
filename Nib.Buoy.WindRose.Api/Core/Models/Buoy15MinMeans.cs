using System;

/*
DROP VIEW IF EXISTS v_means15;
CREATE VIEW v_means15 AS
 SELECT
    p.pid AS Pid, p.dateend AS WindDateEnd,
    IFNULL(ROUND(w.vmspd,2), -99) AS WindMeanSpeed, IFNULL(ROUND(w.vmdir),-99) AS WindMeanDirection, IFNULL(w.beauf,-99) AS WindBeaufortSpeed,
    IFNULL(ROUND(s.meantemp,1), -99) AS SeaSurfaceTemp,
    IFNULL(ROUND(a.tempmean,1), -99) AS AirTemp
    FROM buoy_means.`profile` p
      LEFT JOIN buoy_means.`wind` w ON p.pid=w.pid
      LEFT JOIN buoy_means.sea_water s ON p.pid=s.pid
      LEFT JOIN buoy_means.air a ON p.pid=a.pid
    WHERE p.`period_length`='15'
    ORDER BY p.datestart DESC
    LIMIT 1;
*/
namespace Nib.Buoy.WindRose.Api.Core.Models {
    public class Buoy15MinMeans : IBuoy15MinMeans {
        public uint Pid { get; set; }
        public DateTime WindDateEnd { get; set; }
        public double? WindMeanSpeed { get; set; }
        public double? WindMeanDirection { get; set; }
        public int? WindBeaufortSpeed { get; set; }
        public double? SeaSurfaceTemp { get; set; }
        public double? AirTemp { get; set; }

        public Buoy15MinMeans () { }
    }
}
