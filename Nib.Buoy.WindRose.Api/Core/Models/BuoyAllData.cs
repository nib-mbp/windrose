using System;

namespace Nib.Buoy.WindRose.Api.Core.Models {
    public class BuoyAllData : IBuoy15MinMeans, IBuoy30MinMeans, IWindCurrentData {
        public uint Pid { get; set; }
        public DateTime WindDateEnd { get; set; }
        public DateTime WavesDateEnd { get; set; }
        public double? WindMeanSpeed { get; set; }
        public double? WindMeanDirection { get; set; }
        public int? WindBeaufortSpeed { get; set; }
        public double? SeaSurfaceTemp { get; set; }
        public double? AirTemp { get; set; }
        public double? WavesHeight { get; set; }
        public double? WavesDirection { get; set; }
        public double? WavesPeriod { get; set; }
        public DateTime? ServerDt { get; set; }
        public DateTime? WindCurrentDt { get; set; }
        public double? WindCurrentSpeed { get; set; }
        public double? WindCurrentDirection { get; set; }

        public BuoyAllData (IBuoy15MinMeans buoy15MinMeans, IBuoy30MinMeans buoy30MinMeans, IWindCurrentData windCurrentData) {
            Pid = buoy15MinMeans.Pid;
            WindDateEnd = buoy15MinMeans.WindDateEnd;
            WindMeanSpeed = buoy15MinMeans.WindMeanSpeed;
            WindMeanDirection = buoy15MinMeans.WindMeanDirection;
            WindBeaufortSpeed = buoy15MinMeans.WindBeaufortSpeed;
            SeaSurfaceTemp = buoy15MinMeans.SeaSurfaceTemp;
            AirTemp = buoy15MinMeans.AirTemp;
            WavesDateEnd = buoy30MinMeans.WavesDateEnd;
            WavesHeight = buoy30MinMeans.WavesHeight;
            WavesDirection = buoy30MinMeans.WavesDirection;
            WavesPeriod = buoy30MinMeans.WavesPeriod;
            ServerDt = windCurrentData.ServerDt;
            WindCurrentDt = windCurrentData.WindCurrentDt;
            WindCurrentSpeed = windCurrentData.WindCurrentSpeed;
            WindCurrentDirection = windCurrentData.WindCurrentDirection;
        }
    }
}