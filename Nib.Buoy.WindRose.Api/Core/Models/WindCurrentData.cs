using System;

namespace Nib.Buoy.WindRose.Api.Core.Models {
    public class WindCurrentData : IWindCurrentData {
        public DateTime? ServerDt { get; set; }
        public DateTime? WindCurrentDt { get; set; }
        public double? WindCurrentSpeed { get; set; }
        public double? WindCurrentDirection { get; set; }

        public WindCurrentData () { }
    }
}