using System;

namespace Nib.Buoy.WindRose.Api.Core.Models {
    public interface IBuoy30MinMeans {
        DateTime WavesDateEnd { get; set; }
        double? WavesHeight { get; set; }
        double? WavesDirection { get; set; }
        double? WavesPeriod { get; set; }
    }
}
