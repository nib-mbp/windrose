using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Nib.Buoy.WindRose.Api.Core.Models;

namespace Nib.Buoy.WindRose.Api.Core {
    public class BuoyDbContext : DbContext {
        // private readonly ILoggerFactory _loggerFactory;

        public DbQuery<Buoy15MinMeans> Buoy15MinMeans { get; set; }
        public DbQuery<Buoy30MinMeans> Buoy30MinMeans { get; set; }
        public DbQuery<WindCurrentData> WindCurrentData { get; set; }

        public BuoyDbContext (
            DbContextOptions<BuoyDbContext> options
            // , ILoggerFactory loggerFactory
        ) : base (options) {
            // this._loggerFactory = loggerFactory;
        }

        protected override void OnModelCreating (ModelBuilder modelBuilder) {
            DateTime initDt = DateTime.Parse ("2000-01-01 00:00:00Z");
            DateTime infinityDt = DateTime.Parse ("2099-12-31 23:59:59Z");

            string initDtSql = "GETDATE()";
            if (Database.IsSqlServer ()) {
                // System.Console.WriteLine ("DB provider -> MS SQL Server");
                initDtSql = "GETDATE()";
            } else if (Database.IsMySql ()) {
                System.Console.WriteLine ("DB provider -> MySQL");
                initDtSql = "NOW()";
            }

            modelBuilder.Query<Buoy15MinMeans> ().ToView ("v_means15");
            modelBuilder.Query<Buoy30MinMeans> ().ToView ("v_means30");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            // optionsBuilder.UseLoggerFactory(_loggerFactory);
        }
    }
}
