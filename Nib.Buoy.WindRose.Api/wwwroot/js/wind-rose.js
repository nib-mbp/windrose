// -- Global variables --
var speedMs2KnotsFactor = 1.943844;
var dimensionScale = 1;
// labels
var labelSizeBig = 75;
var labelSizeSmall = 40;
var labelSizeLittle = 24;
var labelSizeBeaufort = 65;
// wind current
var labelWindCurrentX = 320;
var labelWindCurrentY = 100;
// wind average
var labelWindAvgDirX = 320;
var labelWindAvgDirY = 155;
var labelWindAvgSpdX = 658;
var labelWindAvgSpdY = 155;
var labelWindSpeedBeaufortX = 940;
var labelWindSpeedBeaufortY = 920;
var scaleBeaufortX = 850; // 907
var scaleBeaufortY = 35;
// time
var labelTimeX = 97;
var labelTimeY = 747;
// temperature
var labelTempAirX = 320;
var labelTempAirY = 905;
var labelTempSeaX = 658;
var labelTempSeaY = 905;
// waves
var wavesDirX = 320;
var wavesDirY = 968;
var wavesHeightX = 658;
var wavesHeightY = 968;

var canvasCtx = null;
var availableSizeWidth = 0;
var availableSizeHeight = 0;
var selectedSize = 0;
var fullyLoadedImages = 0;
var allImages = 1;

var windSpeedCurrent = 0;
var windSpeedAvg = 0;
var windSpeedBeaufort = 0;
var windDirectionCurrent = 0;
var windDirectionAvg = 0;
var temperatureWater = 0;
var temperatureAir = 0;
var wavesAvgDirection = 0;
var wavesHeight = 0;
var currentTime = '';

var offlineModalVisible = false;

function roundDecimal1(value) {
    num = Math.round(value * 10) / 10;
    numLength = num.toString().length;
    if (num % 1 == 0) {
        numLength += 1;
    } else {
        numLength -= 1;
    }
    return num.toPrecision(numLength);
}

function updateWindRose(jsonObj) {
    // -- Check if data is current --
    let isDataCurrent = true;
    let srvEpochDt = Math.floor((new Date(jsonObj.serverDt + "+00:00")).getTime() / 1000);
    let windEpochDt = Math.floor((new Date(jsonObj.windCurrentDt)).getTime() / 1000);
    if (Math.abs(srvEpochDt - windEpochDt) > 3600) {
        // difference between server date and wind date is greater than 1 hour
        isDataCurrent = false;
        jsonObj.windCurrentDirection = -99;
        jsonObj.windCurrentSpeed = -99;
        if (offlineModalVisible == false) {
            $("#modal-popup").modal();
            offlineModalVisible = true;
        }
    } else {
        if (offlineModalVisible == true) {
            $.modal.close();
            offlineModalVisible = false;
        }
    }

    windSpeedCurrent = (jsonObj.windCurrentSpeed !== -99) ? roundDecimal1(jsonObj.windCurrentSpeed * speedMs2KnotsFactor) : 'n/a';
    windSpeedAvg = (jsonObj.windMeanSpeed !== -99) ? roundDecimal1(jsonObj.windMeanSpeed * speedMs2KnotsFactor) : 'n/a';
    windSpeedBeaufort = (jsonObj.windBeaufortSpeed !== -99) ? jsonObj.windBeaufortSpeed : 0;
    windDirectionCurrent = (jsonObj.windCurrentDirection !== -99) ? jsonObj.windCurrentDirection : 0; // 'n/a';
    windDirectionAvg = (jsonObj.windMeanDirection !== -99) ? jsonObj.windMeanDirection : 0; //'n/a';
    temperatureWater = (jsonObj.seaSurfaceTemp !== -99) ? jsonObj.seaSurfaceTemp : 'n/a';
    temperatureAir = (jsonObj.airTemp !== -99) ? jsonObj.airTemp : 'n/a';
    wavesAvgDirection = (jsonObj.wavesDirection !== -99) ? jsonObj.wavesDirection : 'n/a';
    wavesHeight = (jsonObj.wavesHeight !== -99) ? jsonObj.wavesHeight : 'n/a';
    wavesPeriod = (jsonObj.wavesHeight !== -99) ? jsonObj.wavesHeight : 'n/a';
    if (isDataCurrent) {
        currentTimeObj = new Date(jsonObj.windCurrentDt);
        currentTime = currentTimeObj.toLocaleString().slice(12,20);
    } else {
        currentTime = 'OFFLINE';
    }

    // clear the canvas
    canvasCtx.clearRect(0, 0, selectedSize, selectedSize);

    // -- rotate the img layers --
    var rotationAngleAvg = 180 + windDirectionAvg;
    var rotationAngleCurrent = 180 + windDirectionCurrent;
    $('#windrose-average').rotate(rotationAngleAvg);
    $('#windrose-direction').rotate(rotationAngleCurrent);

    // -- draw the text --
    x = Math.round(labelWindCurrentX * dimensionScale);
    y = Math.round(labelWindCurrentY * dimensionScale);
    labelStr = '' + windSpeedCurrent;
    canvasCtx.font = "normal normal 600 " + labelSizeBig + "px 'Open Sans'"
    canvasCtx.textAlign = 'right';
    canvasCtx.fillStyle = 'black';
    canvasCtx.fillText(labelStr, x, y);

    x = Math.round(labelWindAvgSpdX * dimensionScale);
    y = Math.round(labelWindAvgSpdY * dimensionScale);
    labelStr = '' + windSpeedAvg;
    canvasCtx.font = "normal normal 600 " + labelSizeSmall + "px 'Open Sans'"
    canvasCtx.textAlign = 'right';
    canvasCtx.fillText(labelStr, x, y);

    x = Math.round(labelWindAvgDirX * dimensionScale);
    y = Math.round(labelWindAvgDirY * dimensionScale);
    labelStr = '' + windDirectionAvg;
    canvasCtx.font = "normal normal 600 " + labelSizeSmall + "px 'Open Sans'"
    canvasCtx.textAlign = 'right';
    canvasCtx.fillText(labelStr, x, y);

    x = Math.round(labelTempAirX * dimensionScale);
    y = Math.round(labelTempAirY * dimensionScale);
    labelStr = '' + temperatureAir;
    canvasCtx.font = "normal normal 600 " + labelSizeSmall + "px 'Open Sans'"
    canvasCtx.textAlign = 'right';
    canvasCtx.fillText(labelStr, x, y);

    x = Math.round(labelTempSeaX * dimensionScale);
    y = Math.round(labelTempSeaY * dimensionScale);
    labelStr = '' + temperatureWater;
    canvasCtx.font = "normal normal 600 " + labelSizeSmall + "px 'Open Sans'"
    canvasCtx.textAlign = 'right';
    canvasCtx.fillText(labelStr, x, y);

    x = Math.round(wavesDirX * dimensionScale);
    y = Math.round(wavesDirY * dimensionScale);
    labelStr = '' + wavesAvgDirection;
    canvasCtx.font = "normal normal 600 " + labelSizeSmall + "px 'Open Sans'"
    canvasCtx.textAlign = 'right';
    canvasCtx.fillText(labelStr, x, y);

    x = Math.round(wavesHeightX * dimensionScale);
    y = Math.round(wavesHeightY * dimensionScale);
    labelStr = '' + wavesHeight;
    canvasCtx.font = "normal normal 600 " + labelSizeSmall + "px 'Open Sans'"
    canvasCtx.textAlign = 'right';
    canvasCtx.fillText(labelStr, x, y);

    x = Math.round(labelTimeX * dimensionScale);
    y = Math.round(labelTimeY * dimensionScale);
    labelStr = '' + currentTime;
    canvasCtx.font = "normal normal 600 " + labelSizeLittle + "px 'Open Sans'"
    canvasCtx.textAlign = 'center';
    canvasCtx.fillText(labelStr, x, y);

    x = Math.round(labelWindSpeedBeaufortX * dimensionScale);
    y = Math.round(labelWindSpeedBeaufortY * dimensionScale);
    labelStr = '' + windSpeedBeaufort;
    canvasCtx.font = "normal normal 600 " + labelSizeBeaufort + "px 'Open Sans'"
    canvasCtx.textAlign = 'right';
    canvasCtx.fillStyle = 'white';
    canvasCtx.fillText(labelStr, x, y);

    windBeaufortMeterImg = document.getElementById('windrose-beaufort-meter');
    windBeaufortMeterImg.hidden = false;
    heightPx = Math.round(11 * windSpeedCurrent * dimensionScale);
    topPx = Math.round(803 * dimensionScale) - heightPx;
    windBeaufortMeterImg.style.width = Math.round(scaleBeaufortY * dimensionScale) + 'px';
    windBeaufortMeterImg.style.left = Math.round(scaleBeaufortX * dimensionScale) + 'px';
    windBeaufortMeterImg.style.height = heightPx + 'px';
    windBeaufortMeterImg.style.top = topPx + 'px';

    return;
}
function displayText() {
    canvasCtx.textAlign = 'left';
    canvasCtx.font = 'Tangerine';
    canvasCtx.fillStyle = '#f000f0';
    canvasCtx.fillText('To je test', 2, 50);

    canvasCtx.font = '1.8em "Open Sans"';
    canvasCtx.fillStyle = '#ff0000';
    canvasCtx.fillText('To je test', 2, 150);
}


$(document).ready(function () {
    var div = document.getElementById('wind-rose-container');
    var canvas = document.getElementById('wind-rose-canvas');

    availableSizeWidth = div.scrollWidth;
    availableSizeHeight = div.scrollHeight;
    if (availableSizeWidth <= availableSizeHeight) {
        selectedSize = availableSizeWidth;
    } else {
        selectedSize = availableSizeHeight;
    }
    dimensionScale = selectedSize / 1000;
    labelSizeBig = Math.round(labelSizeBig * dimensionScale);
    labelSizeSmall = Math.round(labelSizeSmall * dimensionScale);
    labelSizeLittle = Math.round(labelSizeLittle * dimensionScale);

    canvas.width = selectedSize;
    canvas.height = selectedSize;

    // store image references into global variables
    var backgroundImg = document.getElementById('windrose-background');
    var windDirectionImg = document.getElementById('windrose-direction');
    var windAverageImg = document.getElementById('windrose-average');
    var windCompassImg = document.getElementById('windrose-compass');
    var helpLinkImg = document.getElementById('help-link-img');

    // set size of images and display them
    backgroundImg.width = backgroundImg.height = selectedSize;
    windDirectionImg.width = windDirectionImg.height = selectedSize;
    windAverageImg.width = windAverageImg.height = selectedSize;
    windCompassImg.width = windCompassImg.height = selectedSize;
    helpLinkImg.width = Math.round(100 * dimensionScale);
    helpLinkImg.height = Math.round(100 * dimensionScale);
    backgroundImg.hidden = false;
    windDirectionImg.hidden = false;
    windAverageImg.hidden = false;
    windCompassImg.hidden = false;

    helpImg = $('#help-link-img');
    helpImg.css({ "left": (backgroundImg.width - helpLinkImg.width) });
    helpLinkImg.hidden = false;

    if (canvas.getContext('2d')) {
        canvasCtx = canvas.getContext('2d');
        let connection = new signalR.HubConnectionBuilder()
            .withUrl("../ws")
            .build();

        connection.start().then(function () { });

        connection.on("data", function (data) {
            updateWindRose(data);
        });

    } else {
        alert("Canvas not supported");
    }
});
