<?php
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Fri, 01 Jan 2010 05:00:00 GMT");
header("Pragma: no-cache");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

$images = array();
$images['background']           = 'img/windrose_background.png';
$images['compass']              = 'img/windrose_compass.png';
$images['directionAverage']     = 'img/windrose_average.png';
$images['directionCurrent']     = 'img/windrose_direction.png';
$images['beaufortOverlay']      = 'img/meter_60.png';
$images['helpButton']           = 'img/help_button.png';

foreach ($images as $key => $value) {
  $fileMTime = @filemtime($images[$key]);
  if (!$fileMTime) {
    $fileMTime = time();
  }
  $images[$key] = $images[$key] . '?' . $fileMTime;
}

readfile('default.htm');
