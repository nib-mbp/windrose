# NIB / MBP - Wind Rose

This is Wind.Rose, an application for displaying real-time wind and other oceanographic data from
*Oceanographic Buoy Vida* (hereafter referred as to *buoy*) in a web browser. The buoy is operated
by National Institute of Biology, Marine Biology station Piran - for more information check
[their web page](https://www.nib.si).

Wind.Rose is a responsive web application and therefore it runs both on mobiles, tablets and on
desktops.

The application comprise of:

- .NET Core back-end (hereafter referred as *BE*),
- HTML5 + JavaScript (jQuery) front-end (hereafter referred as *FE*).

FE relies on data exposed by the BE on websocker protocol.

## Prerequisites

- `.NET Core 2.2 Runtime`;
- `.NET Core 2.2 SDK` (for development only);
- `Docker` for building and running the BE;
- A web server for serving static content and acting as a reverse-proxy in front of the BE. <br>
  On Linux I suggest using `NGINX` or `Apache`.

## Build Instructions

The back-end is designed to be run from *Docker*. For this reason, a `Dockerfile` is supplied for
building the BE and packing it into a Docker image.

### Docker (default)

Docker is the suggested way of building and also running the Wind.Rose application. Use following
commands to build the application and pack it as Docker image:

```bash
docker build --tag www.nib.si/windrose --file Dockerfile  .
```

### Manual build (mostly for development)

Still, sometimes it may come handy to build BE manually. This can be done as follows:

```bash
cd Nib.Buoy.WindRose.Api/
dotnet restore
dotnet publish -c Release -o ../out/
```

## Deploy Instructions

### Preconditions

**TBD**: Database, etc.

### Backend (BE)

The suggested and supported way of running the BE is from Docker. After building an image, it
can spawned in Docker by using a command similar to the below one.

Please note the default configuration doesn't include a DB connection string. Therefore it
has to be defined as a container environment variable `ConnectionStrings__Default` - adopt it
to the target environment / setup.

Use the below command to start the container, expose it on port 4200 and map logging directory
to the host directory `/var/log/nib-wind.rose` (**attention**: adopt the connection string).
```bash
docker run \
  --name "wind-rose-be" \
  --hostname "wind-rose-be" \
  -p 4200:80 \
  -v "/var/log/nib-wind.rose:/app/logs" \
  -e "ConnectionStrings__Default=Server=mysql-db-server.example.com; Port=3306; Database=database; User=username; Password=password" \
  -e "NIB_WINDROSE_LOGS_DIR=/app/logs" \
  www.nib.si/windrose
```

#### Deployment as per 2025-02-14

The deployment is defined in the `/opt/windrose/deploy/docker-deploy.env` file. Here is the content of the file. Please **NOTE** the DB_CONN_STRING has to be updated.
```bash
IMAGE=registry.gitlab.com/nib-mbp/windrose

DB_CONN_STRING="ConnectionStrings__Default=Server=buoylink.nib.si; Port=3306; Database=buoy_data_4_web; User=web_data_access; Password=#-- UPDATE THE PASS --#"
APP_HOST_LOGS_DIR="/var/log/nib-wind.rose"
APP_DOCKER_LOG_DIR="/app/logs"
CONTAINER_NAME="wind-rose-be"

if [[ -z $IMAGE_TAG ]]; then
  echo "ERROR: 'IMAGE_TAG' environment is not set! Exiting ..."
else
  cat <<_EOF_
Please run:
  docker pull ${IMAGE}:${IMAGE_TAG}
  docker kill "${CONTAINER_NAME}"
  docker rm "${CONTAINER_NAME}"
  docker run -d \\
    --name "${CONTAINER_NAME}" \\
    --hostname "${CONTAINER_NAME}" \\
    -p 5001:80 \\
    -v "${APP_HOST_LOGS_DIR}:${APP_DOCKER_LOG_DIR}" \\
    -e "${DB_CONN_STRING}" \\
    -e "NIB_WINDROSE_LOGS_DIR=${APP_DOCKER_LOG_DIR}" \\
    -e "NIB_WINDROSE_WS_ROOT=/mbp/apps/wind-rose" \\
    ${IMAGE}:${IMAGE_TAG}
_EOF_
fi
```

To re-deploy just run the following and you'll get all instructions needed:
```bash
. /opt/windrose/deploy/docker-deploy.env
```

### FE

The FE application can be found in `Nib.Buoy.WindRose.Api/wwwroot/`. Just copy the content of the
directory to a web server root and set a reverse-proxy to map `/ws/` to the Docker mapped port.

## Development

Quick reference for developers - running the service locally.

**ATTENTION:** Below commands should be executed from the `Nib.Buoy.WindRose.Api/` directory.

### Build and run instructions

#### OS specifics

##### MacOS / Linux Specific
```bash
export ASPNETCORE_ENVIRONMENT=Development

export DB_HOST=abc.example.com
export DB_PORT=3306
export DB_USER=user_name
export DB_PASS=a123b456
export DB_DB=buoy_data_4_web
export ConnectionStrings__Default=Server=${DB_HOST}; Port=${DB_PORT}; Database=${DB_DB}; User=${DB_USER}; Password=${DB_PASS};
```

##### Windows Specific

**CMD**
```cmd
set ASPNETCORE_ENVIRONMENT=Development

set DB_HOST=abc.example.com
set DB_PORT=3306
set DB_USER=user_name
set DB_PASS=a123b456
set DB_DB=buoy_data_4_web
set ConnectionStrings__Default=Server=%DB_HOST%; Port=%DB_PORT%; Database=%DB_DB%; User=%DB_USER%; Password=%DB_PASS%;
```

**PowerShell**
```powershell
$Env:ASPNETCORE_ENVIRONMENT = "Development"
$Env:DB_HOST = "abc.example.com"
$Env:DB_PORT = 3306
$Env:DB_USER = "user_name"
$Env:DB_PASS = "a123b456"
$Env:DB_DB = "buoy_data_4_web"
$Env:ConnectionStrings__Default = "Server=$Env:DB_HOST; Port=$Env:DB_PORT; Database=$Env:DB_DB; User=$Env:DB_USER; Password=$Env:DB_PASS;"
```

#### Common

```bash
dotnet watch run
```

### Development and Debugging

#### Hints

- In order to display EF Core generated SQL queries in logs, increase the `Microsoft.EntityFrameworkCore.Database.Command` module log level in `appsettings.json` as follows:
  ```json
  "Microsoft.EntityFrameworkCore.Database.Command": "Warning",
  ```
