## Build the .NET Core app
FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-env
WORKDIR /app

COPY Nib.Buoy.WindRose.Api/*.csproj ./
RUN dotnet restore

COPY Nib.Buoy.WindRose.Api/. ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2
WORKDIR /app
COPY --from=build-env /app/out .
RUN mkdir -p ./logs
ENTRYPOINT ["dotnet", "Nib.Buoy.WindRose.dll"]
EXPOSE 80
